B
    �e�`~d  �               @   s  d Z ddlmZmZ ddlZddlZyddlm  m	Z
 W n  ek
rZ   ddlm
Z
 Y nX ddlm  mZ ddlmZ dd� Zdd� Zd-d	d
�Zd.dd�Zd/dd�Zd0dd�Zd1dd�Zdd� Zd2dd�Zd3dd�Zd4dd�Zd5d"d#�Zd$d%� Zd&Z d'd(� Z!d6d)d*�Z"d7d+d,�Z#dS )8z2
FFT based image registration. --- main functions
�    )�division�print_functionNc             C   s�   t �t j d t jd | d �dd�t jf }t �t j d t jd | d �t jdd�f }t �|d |d  �}dt �|�d  }d|t �|�t jd k< |S )z�
    Make a radial cosine filter for the logpolar transform.
    This filter suppresses low frequencies and completely removes
    the zero freq.
    g       @r   N�   �   g      �?)�np�linspace�pi�newaxis�sqrt�cos�abs)�shape�yy�xx�rads�filt� r   �D/Users/jk/Programs/Python/Current/Demos/Demo4/imreg_dft_JMK/imreg.py�_logpolar_filter4   s    ..r   c             C   s   t t| �d �fd }|S )Ng      �?r   )�int�max)r   �retr   r   r   �_get_pcorr_shapeE   s    r   �infc                s�  t | �dkstd��| d j}dd� | D �}dd� |D �}t|�� � fdd�|D �}t|��t|�d ����fd	d�|D �}t|d |d tj�|||�\\}	}
}t	j
 |	 t�d � }t	�|�}t�|d
�}�|
 }| }d| }|dk	�r�� j|d< �|d< |�d��r||d< |�d��r6dd� |D �|d< |�d��rJ||d< |�d��r�|	|
f|d< ||f|d< ||d< �d d }�|  �| ddf|d< d|  k �r�dk �s�n td | ��||fS )!a�  
    Given two images, return their scale and angle difference.

    Args:
        ims (2-tuple-like of 2D ndarrays): The images
        bgval: We also pad here in the :func:`map_coordinates`
        exponent (float or 'inf'): The exponent stuff, see :func:`similarity`
        constraints (dict, optional)
        reports (optional)

    Returns:
        tuple: Scale, angle. Describes the relationship of
        the subject image to the first one.
    r   z&Only two images are supported as inputr   c             S   s   g | ]}t �|��qS r   )�utils�_apodize)�.0�imr   r   r   �
<listcomp>]   s    z"_get_ang_scale.<locals>.<listcomp>c             S   s   g | ]}t �t �|���qS r   )�fft�fftshift�fft2)r   r   r   r   r   r   ^   s    c                s   g | ]}|�  �qS r   r   )r   �dft)r   r   r   r   `   s    r   c                s   g | ]}t t�|��� ��qS r   )�	_logpolarr   r   )r   r"   )�log_base�pcorr_shaper   r   r   g   s   ih  g      �?Nr   �base�spectra�	dfts_filt�inputsc             S   s   g | ]}t �tj �|���qS r   )r   �ifft2r   �	ifftshift)r   r"   r   r   r   r   }   s   �ims_filt�logpolar�	logpolars�scale_anglezamas-result-rawzamas-resultzamas-successg       @i�����Z   zamas-extentg      �?�   z>Images are not compatible. Scale change %g too big to be true.)�len�AssertionErrorr   r   r   �_get_log_base�_phase_correlationr   �argmax_angscaler   r   �float�rad2deg�
wrap_angle�show�
ValueError)�ims�bgval�exponent�constraints�reportsr   �ims_apod�dfts�stuffs�arg_ang�arg_rad�success�angle�scale�	extent_elr   )r   r$   r%   r   �_get_ang_scaleJ   sT    



rJ   r   c             C   sh  d}d }}|dk	r.|� d�r.|�� }|�� }t| ||||�\}	}
t| t�|�|||�\}}d}|| |
kst|dkrxd}|dk	r�|� d�r�|d |d< |d	 |d
< |
|d< t|	�|d< |d |d< |d	 |d< ||d< t|�|d< |dk	�r@|� d��r@tt�|�|ddd�t||	ddd�g}|�r2|ddd� }|d �|� |�rV|}	|}
|d7 }t|	|
|d�}|S )a-  
    Return translation vector to register images.
    It tells how to translate the im1 to get im0.

    Args:
        im0 (2D numpy array): The first (template) image
        im1 (2D numpy array): The second (subject) image
        filter_pcorr (int): Radius of the minimum spectrum filter
            for translation detection, use the filter when detection fails.
            Values > 3 are likely not useful.
        constraints (dict or None): Specify preference of seeked values.
            For more detailed documentation, refer to :func:`similarity`.
            The only difference is that here, only keys ``tx`` and/or ``ty``
            (i.e. both or any of them or none of them) are used.
        odds (float): The greater the odds are, the higher is the preferrence
            of the angle + 180 over the original angle. Odds of -1 are the same
            as inifinity.
            The value 1 is neutral, the converse of 2 is 1 / 2 etc.

    Returns:
        dict: Contains following keys: ``angle``, ``tvec`` (Y, X),
            and ``success``.
    r   N�translationF�����Tzamt-origzt0-origzamt-postproczt0-postprocz
t0-successzt0-tveczt1-origzt1-postprocz
t1-successzt1-tvec�transformed�wrap�   )�tvec�mode�order�after_tform�   )rP   rF   rG   )	r:   �
copy_empty�_translationr   �rot180�tuple�transform_img�extend�dict)�im0�im1�filter_pcorr�oddsr?   r@   rG   �
report_one�
report_tworP   �succ�tvec2�succ2�pick_rotated�toappr   r   r   r   rK   �   s@    rK   c             C   s>   t | �}t| |d �}||d  d }d|d  d }||fS )z�
    Given the parameters of the log-polar transform, get width of the interval
    where the correct values are.

    Args:
        shape (tuple): Shape of images
        scale (float): The scale difference (precision varies)
    r   g      �?g     �f@r   )r   r4   )r   rH   r%   r$   �Dscale�Dangler   r   r   �_get_precision�   s
    	ri   rO   c	             C   s�  |dkrt �|d�}| j}	|	|jkr.td��n| jdkr@td��d}
d}|}tddgd	dgd
�}|dkrl|}|�|� |}|�� }t|d �|d< t|d �|d< |dk	r�|�	d�r�|�� g|d< x�t
|�D ]�}t| |g||||�\}}|
|9 }
||7 }|d d  |  < |d d  |8  < t||
|||d�}|dk	r�|�	d�r�|d �|�� � q�W |�dd�\}}t|||�}t| |||||�}||d 7 }t �|d�|d< t|	|
�\}}|
|d< ||d< ||d< d|d< |S )a�  
    This function takes some input and returns mutual rotation, scale
    and translation.
    It does these things during the process:

    * Handles correct constraints handling (defaults etc.).
    * Performs angle-scale determination iteratively.
      This involves keeping constraints in sync.
    * Performs translation determination.
    * Calculates precision.

    Returns:
        Dictionary with results.
    N�   zImages must have same shapes.r   zImages must be 2-dimensional.g      �?g        r   r   )rG   rH   rH   rG   rM   rS   )r=   rR   )r   Nih  rg   rh   g      �?�Dt)r   �get_bordervalr   r;   �ndimr[   �update�copy�listr:   �rangerJ   rY   �append�get�	_get_oddsrK   r9   ri   )r\   r]   �numiterrR   r?   r^   r>   r=   r@   r   rH   rG   �im2�constraints_default�constraints_dynamic�ii�newscale�newangle�target�stdevr_   �resrh   rg   r   r   r   �_similarity�   sT    




r   c          
   C   sl   t �|d�}t| ||||||||�	}	t||	||�}
tt�|�|	dd�}d||dk< t �|
|d�}||	d< |	S )a�  
    Return similarity transformed image im1 and transformation parameters.
    Transformation parameters are: isotropic scale factor, rotation angle (in
    degrees), and translation vector.

    A similarity transformation is an affine transformation with isotropic
    scale and without shear.

    Args:
        im0 (2D numpy array): The first (template) image
        im1 (2D numpy array): The second (subject) image
        numiter (int): How many times to iterate when determining scale and
            rotation
        order (int): Order of approximation (when doing transformations). 1 =
            linear, 3 = cubic etc.
        filter_pcorr (int): Radius of a spectrum filter for translation
            detection
        exponent (float or 'inf'): The exponent value used during processing.
            Refer to the docs for a thorough explanation. Generally, pass "inf"
            when feeling conservative. Otherwise, experiment, values below 5
            are not even supposed to work.
        constraints (dict or None): Specify preference of seeked values.
            Pass None (default) for no constraints, otherwise pass a dict with
            keys ``angle``, ``scale``, ``tx`` and/or ``ty`` (i.e. you can pass
            all, some of them or none of them, all is fine). The value of a key
            is supposed to be a mutable 2-tuple (e.g. a list), where the first
            value is related to the constraint center and the second one to
            softness of the constraint (the higher is the number,
            the more soft a constraint is).

            More specifically, constraints may be regarded as weights
            in form of a shifted Gaussian curve.
            However, for precise meaning of keys and values,
            see the documentation section :ref:`constraints`.
            Names of dictionary keys map to names of command-line arguments.

    Returns:
        dict: Contains following keys: ``scale``, ``angle``, ``tvec`` (Y, X),
        ``success`` and ``timg`` (the transformed subject image)

    .. note:: There are limitations

        * Scale change must be less than 2.
        * No subpixel precision (but you can use *resampling* to get
          around this).
    rj   r   r   g      �?g�������?�
   �timg)r   rl   r   �transform_img_dictr   �	ones_like�	frame_img)r\   r]   ru   rR   r?   r^   r>   r@   r=   r~   rv   �imask�im3r   r   r   �
similarityI  s    0r�   c                s�   d}� dk	r�dd� ||  ||  d fD �}d\}}� dkrP� fdd�|D �\}}|dkrf|dkrfd	}n:� dks~|dkr�|dkr�d	}|d |d k r�d}n|| }|S )
u?  
    Determine whether we are more likely to choose the angle, or angle + 180°

    Args:
        angle (float, degrees): The base angle.
        target (float, degrees): The angle we think is the right one.
            Typically, we take this from constraints.
        stdev (float, degrees): The relevance of the target value.
            Also typically taken from constraints.

    Return:
        float: The greater the odds are, the higher is the preferrence
            of the angle + 180 over the original angle. Odds of -1 are the same
            as inifinity.
    r   Nc             S   s   g | ]}t t�|d ���qS )ih  )r   r   r9   )r   �angr   r   r   r   �  s   z_get_odds.<locals>.<listcomp>rT   )r   r   r   c                s$   g | ]}t �|d   � d   ��qS )r   )r   �exp)r   �diff)r}   r   r   r   �  s    rL   r   )rG   r|   r}   r   �diffs�odds0�odds1r   )r}   r   rt   �  s    rt   c             C   s    t | |tj|||�\}}||fS )zK
    The plain wrapper for translation phase correlation, no big deal.
    )r5   r   �argmax_translation)r\   r]   r^   r?   r@   r   rb   r   r   r   rV   �  s    rV   c             G   s�   |dkrt j}dd� | |fD �\}}t|��� d }tt�||��  t|�t|� |  ��}t�|�}||f|�� \\}	}
}t�	|	|
f�}|	|j
d d 8 }	|
|j
d d 8 }
|t�	|j
t�d 8 }||fS )a�  
    Computes phase correlation between im0 and im1

    Args:
        im0
        im1
        callback (function): Process the cross-power spectrum (i.e. choose
            coordinates of the best element, usually of the highest one).
            Defaults to :func:`imreg_dft.utils.argmax2D`

    Returns:
        tuple: The translation vector (Y, X). Translation vector of (0, 0)
            means that the two images match.
    Nc             S   s   g | ]}t �|��qS r   )r   r!   )r   �arrr   r   r   r   �  s    z&_phase_correlation.<locals>.<listcomp>gV瞯�<r   r   r   )r   �argmax2Dr   r   r   r*   �	conjugater    r   �arrayr   r   )r\   r]   �callback�args�f0�f1�eps�cps�scps�t0�t1rF   r   r   r   r   r5   �  s    *
r5   Fc       	      C   sR   |d }|d }t �|d �}|r:d| }|d9 }|d9 }t| |||||d�}|S )a�  
    Wrapper of :func:`transform_img`, works well with the :func:`similarity`
    output.

    Args:
        img
        tdict (dictionary): Transformation dictionary --- supposed to contain
            keys "scale", "angle" and "tvec"
        bgval
        order
        invert (bool): Whether to perform inverse transformation --- doesn't
            work very well with the translation.

    Returns:
        np.ndarray: .. seealso:: :func:`transform_img`
    rH   rG   rP   g      �?rL   )r=   rR   )r   r�   rY   )	�img�tdictr=   rR   �invertrH   rG   rP   r~   r   r   r   r�   �  s    r�   �      �?�        �r   r   �constantc          	   C   s4  | j dkr^t�| �}xDt| jd �D ]2}td�td�|f}	t| |	 ||||||�||	< q$W |S |dkrpt�| �}t�	t�
| j�d ��t�}
t�|
| j�| }t�|| �� �}|dkr�tj|||||d�}|dkr�tj|||||d�}|d dk�s|d	 dk�rtj|||||d�}t�| �| }t�||�}|S )
aI  
    Return translation vector to register images.

    Args:
        img (2D or 3D numpy array): What will be transformed.
            If a 3D array is passed, it is treated in a manner in which RGB
            images are supposed to be handled - i.e. assume that coordinates
            are (Y, X, channels).
        scale (float): The scale factor (scale > 1.0 means zooming in)
        angle (float): Degrees of rotation (clock-wise)
        tvec (2-tuple): Pixel translation vector, Y and X component.
        mode (string): The transformation mode (refer to e.g.
            :func:`scipy.ndimage.shift` and its kwarg ``mode``).
        bgval (float): Shade of the background (filling during transformations)
            If None is passed, :func:`imreg_dft.utils.get_borderval` with
            radius of 5 is used to get it.
        order (int): Order of approximation (when doing transformations). 1 =
            linear, 3 = cubic etc. Linear works surprisingly well.

    Returns:
        np.ndarray: The transformed img, may have another
        i.e. (bigger) shape than the source.
    rO   r   Ng333333�?g      �?)rR   rQ   �cvalg        r   r   )rm   r   �
empty_likerq   r   �slicerY   r   rl   �roundr�   �astyper   �zeros�dtype�embed_toro   �ndii�zoom�rotate�shift�
zeros_like)r�   rH   rG   rP   rQ   r=   rR   r   �idx�sli�bigshape�bg�dest0�destr   r   r   rY   �  s*    


rY   c             C   s�   t d��t�| | dg�}t�d�}t�|�}t�|�|d< t�|�|d< t�|� |d< t�|�|d< t�d�}||dd	�d	f< t�|t�||��S )
a  
    Return homogeneous transformation matrix from similarity parameters.

    Transformation parameters are: isotropic scale factor, rotation angle (in
    degrees), and translation vector (of size 2).

    The order of transformations is: scale, rotate, translate.

    z+We have no idea what this is supposed to dog      �?rO   )r   r   )r   r   )r   r   )r   r   Nr   )	�NotImplementedErrorr   �diag�identity�math�radiansr   �sin�dot)rH   rG   �vector�m_scale�m_rot�m_translr   r   r   �similarity_matrix3  s    



r�   g�������?c             C   s,   | d t  }|d }t�t�|�| �}|S )a{  
    Basically common functionality of :func:`_logpolar`
    and :func:`_get_ang_scale`

    This value can be considered fixed, if you want to mess with the logpolar
    transform, mess with the shape.

    Args:
        shape: Shape of the original image.
        new_r (float): The r-size of the log-polar transform array dimension.

    Returns:
        float: Base of the log-polar transform.
        The following holds:
        :math:`log\_base = \exp( \ln [ \mathit{spectrum\_dim} ] / \mathit{loglpolar\_scale\_dim} )`,
        or the equivalent :math:`log\_base^{\mathit{loglpolar\_scale\_dim}} = \mathit{spectrum\_dim}`.
    r   g       @)�EXCESS_CONSTr   r�   �log)r   �new_r�old_rr$   r   r   r   r4   K  s    r4   c             C   s�   |dkrt �| d�}t �| j�}|d d |d d f}t�|�}t�||�}|�� }|d t|d � }	||	 }|t �	|� |d  }
|t �
|� |d  }t �|
�}tj| |
|g|dd|d� |S )a�  
    Return log-polar transformed image
    Takes into account anisotropicity of the freq spectrum
    of rectangular images

    Args:
        image: The image to be transformed
        shape: Shape of the transformed image
        log_base: Parameter of the transformation, get it via
            :func:`_get_log_base`
        bgval: The backround value. If None, use minimum of the image.

    Returns:
        The transformed image
    Nr   r   g       @rO   r�   )�outputrR   rQ   r�   )r   �
percentiler�   r   r   �_get_angles�_get_logradro   r7   r�   r   r�   r�   �map_coordinates)�imager   r$   r=   �imshape�center�theta�radius_x�radius_y�ellipse_coef�y�xr�   r   r   r   r#   h  s    


r#   c             K   s�   ddl m} |dkr|�� }|dkr(d}t�|d�t�| d� }t|| |  �}|�d�}	|	j| |f|� |	��  t	|	|	d�}
|jd|
�}|j||f|� |��  |jd|
�}|j||f|� |��  |jd|
�}|j||f|� |��  |S )a�  
    Plot images using matplotlib.
    Opens a new figure with four subplots:

    ::

      +----------------------+---------------------+
      |                      |                     |
      |   <template image>   |   <subject image>   |
      |                      |                     |
      +----------------------+---------------------+
      | <difference between  |                     |
      |  template and the    |<transformed subject>|
      | transformed subject> |                     |
      +----------------------+---------------------+

    Args:
        im0 (np.ndarray): The template image
        im1 (np.ndarray): The subject image
        im2: The transformed subject --- it is supposed to match the template
        cmap (optional): colormap
        fig (optional): The figure you would like to have this plotted on

    Returns:
        matplotlib figure: The figure with subplots
    r   )�pyplotN�coolwarmg     �X@��   )�sharex�sharey��   ��   ��   )r�   )r�   )r�   )
�
matplotlibr�   �figurer   r�   r   �add_subplot�imshow�gridr[   )r\   r]   rv   �cmap�fig�kwargsr�   �normr�   �pl0�share�plr   r   r   r�   �  s*    
r�   )r   NN)r   r   NN)r   )r   rO   Nr   r   NN)r   rO   Nr   r   N)r   NN)N)Nr   F)r�   r�   r�   r�   Nr   )N)NN)$�__doc__�
__future__r   r   r�   �numpyr   Zpyfftw.interfaces.numpy_fft�
interfacesZ	numpy_fftr   �ImportErrorZ	numpy.fftZscipy.ndimage.interpolation�ndimage�interpolationr�   �imreg_dft.utilsr   r   r   rJ   rK   ri   r   r�   rt   rV   r5   r�   rY   r�   r�   r4   r#   r�   r   r   r   r   �<module>$   s<   
K 
F
 
X 
A#

'
 
5
'