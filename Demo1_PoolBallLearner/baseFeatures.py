balls = [('Root','Sphere','Pool','Solid','Cueball','White'),
         
         ('Root','Sphere','Pool','Solid','1','Yellow'),
         ('Root','Sphere','Pool','Solid','2','Blue'),
         ('Root','Sphere','Pool','Solid','3','Red'),
         ('Root','Sphere','Pool','Solid','4','Indigo'),
         ('Root','Sphere','Pool','Solid','5','Orange'),
         ('Root','Sphere','Pool','Solid','6','Green'),
         ('Root','Sphere','Pool','Solid','7','Maroon'),
         
         ('Root','Sphere','Pool','Solid','8','Black'),
         
         ('Root','Sphere','Pool','Stripe','9','Yellow'),
         ('Root','Sphere','Pool','Stripe','10','Blue'),
         ('Root','Sphere','Pool','Stripe','11','Red'),
         ('Root','Sphere','Pool','Stripe','12','Indigo'),
         ('Root','Sphere','Pool','Stripe','13','Orange'),
         ('Root','Sphere','Pool','Stripe','14','Green'),
         ('Root','Sphere','Pool','Stripe','15','Maroon'),
         ]

bowlingBalls = [ ('Root','Sphere','Bowling','Green'),
                 ('Root','Sphere','Bowling','Red'),
                 ('Root','Sphere','Bowling','Blue'),
                 ('Root','Sphere','Bowling','Indigo'),
                 ('Root','Sphere','Bowling','Polkadot'),
                 ('Root','Sphere','Bowling','Pinstripe'),
         ]


establishments = [tuple(['Root','Establishments'] + [t for t in "Red Roof Inn".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Red Lobster".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Honor Homes".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Blue Water Properies".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Blue Water Baltimore".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Blue Homes Baltimore".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Blue Sky Baltimore".split()]),
                   tuple(['Root','Establishments'] + [t for t in "Blue Sky Movers".split()])
         ]
