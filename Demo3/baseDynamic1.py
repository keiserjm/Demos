import numpy as np
import scipy.special as sc
import cv2 as cv
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu
from scipy import ndimage

import matplotlib.image as mpimg

# import imreg_dft as ird

import imreg_dft_JMK.imreg as ird
import imreg_dft_JMK.utils

from scipy.signal import fftconvolve

from baseIncludesReg1 import *


def initializeScene(numObjs,sceneHeight,sceneWidth, imgHeight,imgWidth, kk,shapeDescs, angles,scales):

    sceneImg = 0*np.ones((sceneHeight,sceneWidth),np.uint8)

    yc,xc = imgHeight//2,imgWidth//2
    x0,y0 = np.int(xc+(sceneWidth-imgWidth)*np.random.random()), np.int(yc+(sceneHeight-imgHeight)*np.random.random())
    x0A = []
    y0A = []
    kkA = []
    for n in range(numObjs):
        k1 = np.random.choice(kk)
        kkA.append(k1)
        obsTmp = shapeDescs[k1]['imag']
        angle = np.random.choice(angles)
        scale = np.random.choice(scales)
        obsImg,obsCnt = transObject(obsTmp,angle,scale, imgHeight,imgWidth,xc,yc)
        obsImg = (1-1*(obsImg[:]>0)).astype(np.uint8)
        sceneImg[y0-yc:y0+yc,x0-xc:x0+xc] = cv.bitwise_or(sceneImg[y0-yc:y0+yc,x0-xc:x0+xc],obsImg)
        sceneImg[y0-5:y0+5,x0-5:x0+5] = 0
    
        if 0:
            print(x0,y0)
            fig,axs = plt.subplots(1,2,figsize=(20,10))
            axs[0].imshow(obsImg)
            axs[1].imshow(sceneImg)
            plt.show()
    
        x0A.append(x0)
        y0A.append(y0)
        while np.min([np.sqrt((x0-x)**2+(y0-y)**2) for x,y in zip(x0A,y0A)]) < np.sqrt(xc**2+yc**2):
            x0,y0 = np.int(xc+(sceneWidth-imgWidth)*np.random.random()), np.int(yc+(sceneHeight-imgHeight)*np.random.random())
    sceneImg *= 255

    return sceneImg, x0A, y0A, kkA


def updateScene(sceneHeight,sceneWidth, imgHeight,imgWidth, tracks,shapeDescs):

    sceneImg = 0*np.ones((sceneHeight,sceneWidth),np.uint8)

    yc,xc = imgHeight//2,imgWidth//2
    for track in tracks:
        k1 = tracks[track]['shape']
        x0 = np.int(tracks[track]['center'][0] + tracks[track]['dx'])
        y0 = np.int(tracks[track]['center'][1] + tracks[track]['dy'])
        angle = tracks[track]['angle']  + tracks[track]['da']
        scale = tracks[track]['scale']  + tracks[track]['ds']

        obsTmp = shapeDescs[k1]['imag']
        obsImg,obsCnt = transObject(obsTmp,angle,scale, imgHeight,imgWidth,xc,yc)
        obsImg = (1-1*(obsImg[:]>0)).astype(np.uint8)
        sceneImg[y0-yc:y0+yc,x0-xc:x0+xc] = cv.bitwise_or(sceneImg[y0-yc:y0+yc,x0-xc:x0+xc],obsImg)

        tracks[track]['center'] = (x0,y0)
        tracks[track]['angle'] = angle
        tracks[track]['scale'] = scale

        sceneImg[y0-5:y0+5,x0-5:x0+5] = 0
    
    sceneImg *= 255

    return sceneImg

