import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
import random
import copy

import itertools

from lexpy2.trie import Trie

import networkx as nx
from networkx.drawing.nx_pydot import graphviz_layout
# from networkx.drawing.nx_agraph import to_agraph
import pydot


def printTree3(root, markerStr="+- ", levelMarkers=[]):
    emptyStr = " "*len(markerStr)
    connectionStr = "|" + emptyStr[:-1]
    level = len(levelMarkers) 
    mapper = lambda draw: connectionStr if draw else emptyStr
    markers = "".join(map(mapper, levelMarkers[:-1]))
    markers += markerStr if level > 0 else ""
    print(f"{markers}{root.val}",end='')
    if root.eow is True:
        print(f"\tCount: {root.count}")
    else:
        print()
    for i, child in enumerate(root.children):
        isLast = i == len(root.children) - 1
        printTree3(root.children[child], markerStr, [*levelMarkers, not isLast])

# https://stackoverflow.com/questions/57512155/how-to-draw-a-tree-more-beautifully-in-networkx
def plotTree1(G,axs):
    #Note: You can also try a spring_layout
    #pos = nx.circular_layout(G)
    pos = graphviz_layout(G, prog="neato") # "dot" or "circo" or "twopi"
    nx.draw_networkx_nodes(G,pos,node_color='green',node_size=50)

    #3. If you want, add labels to the nodes
    labels = {}
    node_list = G.nodes
    for node_name in node_list:
        labels[str(node_name)] =str(node_name)
    nx.draw_networkx_labels(G,pos,labels,font_size=20)

    all_weights = []
    #4 a. Iterate through the graph nodes to gather all the weights
    for (node1,node2,data) in G.edges(data=True):
        all_weights.append(data['weight']) #we'll use this when determining edge thickness

    #4 b. Get unique weights
    unique_weights = list(set(all_weights))

    #4 c. Plot the edges - one by one!
    for weight in unique_weights:
        #4 d. Form a filtered list with just the weight you want to draw
        weighted_edges = [(node1,node2) for (node1,node2,edge_attr) in G.edges(data=True) if edge_attr['weight']==weight]
        #4 e. I think multiplying by [num_nodes/sum(all_weights)] makes the graphs edges look cleaner
        width = weight*len(node_list)*2.0/sum(all_weights)
        nx.draw_networkx_edges(G,pos,edgelist=weighted_edges,width=width)

    #Plot the graph
    plt.axis('off')
    return



# https://www.javaer101.com/en/article/2909608.html

import random

def hierarchy_pos(G, root=None, width=1., vert_gap = 0.2, vert_loc = 0, xcenter = 0.5):

    '''
    From Joel's answer at https://stackoverflow.com/a/29597209/2966723.  
    Licensed under Creative Commons Attribution-Share Alike 

    If the graph is a tree this will return the positions to plot this in a 
    hierarchical layout.

    G: the graph (must be a tree)

    root: the root node of current branch 
    - if the tree is directed and this is not given, 
      the root will be found and used
    - if the tree is directed and this is given, then 
      the positions will be just for the descendants of this node.
    - if the tree is undirected and not given, 
      then a random choice will be used.

    width: horizontal space allocated for this branch - avoids overlap with other branches

    vert_gap: gap between levels of hierarchy

    vert_loc: vertical location of root

    xcenter: horizontal location of root
    '''
    if not nx.is_tree(G):
        raise TypeError('cannot use hierarchy_pos on a graph that is not a tree')

    if root is None:
        if isinstance(G, nx.DiGraph):
            root = next(iter(nx.topological_sort(G)))  #allows back compatibility with nx version 1.11
        else:
            root = random.choice(list(G.nodes))

    def _hierarchy_pos(G, root, width=1., vert_gap = 0.2, vert_loc = 0, xcenter = 0.5, pos = None, parent = None):
        '''
        see hierarchy_pos docstring for most arguments

        pos: a dict saying where all nodes go if they have been assigned
        parent: parent of this branch. - only affects it if non-directed

        '''

        if pos is None:
            pos = {root:(xcenter,vert_loc)}
        else:
            pos[root] = (xcenter, vert_loc)
        children = list(G.neighbors(root))
        if not isinstance(G, nx.DiGraph) and parent is not None:
            children.remove(parent)  
        if len(children)!=0:
            dx = width/len(children) 
            nextx = xcenter - width/2 - dx/2
            for child in children:
                nextx += dx
                pos = _hierarchy_pos(G,child, width = dx, vert_gap = vert_gap, 
                                    vert_loc = vert_loc-vert_gap, xcenter=nextx,
                                    pos=pos, parent = root)
        return pos


    return _hierarchy_pos(G, root, width, vert_gap, vert_loc, xcenter)


def graph_draw(graph,axs,newpos):
    nx.draw(graph,ax=axs,pos=newpos,
         node_size = [16 * graph.degree(n) for n in graph],
         # node_color = [graph.depth[n] for n in graph],
         with_labels = True)
    return


# https://www.geeksforgeeks.org/directed-graphs-multigraphs-and-visualization-in-networkx/
def graph_draw2(graph,axs,newpos,node_scale,edge_scale,fontSize):
    node_color = [graph.degree(v) for v in graph]
    node_size = [node_scale * nx.get_node_attributes(graph, 'count')[v] for v in graph]
    edge_width = [edge_scale * graph[u][v]['weight'] for u, v in graph.edges()]

    retval, = nx.draw_networkx(graph, ax=axs,pos=newpos,node_size = node_size, 
                node_color = node_color, alpha = 0.7,
                with_labels = True, font_size = fontSize, font_weight='bold',width = edge_width,
                edge_color ='.4') # , cmap = plt.cm.Blues)

    return retval


def list_words(trie,delim):
    my_list = []
    for i,child in enumerate(trie.children):
        if trie.children[child].eow == False:
            for el in list_words(trie.children[child],delim):
                my_list.append(child+delim+el)
        else:
            my_list.append(child)
    return my_list

def retrieveData(trie,sentence,delim):
    assert sentence is not None, "Input word cannot be None"
    node = trie.root
    sentenceSplit = sentence.split(delim)
    for i, word in enumerate(sentenceSplit):
        node = node[word]
        if i == len(sentenceSplit)-1:
            return [node.id,node.val,node.eow,node.count]

def thresholdPrune(trie,thresh,delim):
    my_list = []
    for i,child in enumerate(trie.children):
        if trie.children[child].eow == False:
            for el in thresholdPrune(trie.children[child],thresh,delim):
                my_list.append(child+delim+el)
        else:
            if np.int(trie.children[child].count) < thresh:
                my_list.append(child)
            else:
                my_list = []
    return my_list

def firstAboveThresh(v,t):
    for i in range(len(v)):
        if v[i] > t:
            return i+1
    return None


def populateTrie(T,obs,delim,count=1):

    input_phrase = delim.join(['{0:s}'.format(str(r)) for r in obs])
    T.add(input_phrase,delim,count=count)

    return T

def populateGraph(G,obs,delim,iFrame):

    input_phrase = delim.join(['{0:s}'.format(str(r)) for r in obs])
    word_list = input_phrase.split(delim)
    for iWord in range(1,len(word_list)):
        if G.has_edge(word_list[iWord-1],word_list[iWord]):
            G.nodes[word_list[iWord-1]]['count'] += 1
            G.nodes[word_list[iWord]]['count'] += 1
            G[word_list[iWord-1]][word_list[iWord]]['weight'] += 1
            frames = G[word_list[iWord-1]][word_list[iWord]]['frames']
            frames.append(iFrame)
            G[word_list[iWord-1]][word_list[iWord]]['frames'] = frames[-45:]
        else:
            G.add_edges_from([(word_list[iWord-1],word_list[iWord],{'weight':1,'frames':[iFrame]})])
            nx.set_node_attributes(G,{word_list[iWord-1]:{'count':1}})
            nx.set_node_attributes(G,{word_list[iWord  ]:{'count':1}})

    return G

def pruneGraphWeights(G,thresh):
    remove = [(a,b) for a, b, attrs in G.edges(data=True) if attrs["weight"] < thresh]
    G.remove_edges_from(remove)
    return G

def pruneTrieCounts(T,thresh,delim):

    sentences = list_words(T.root,delim)

    cnts = {}
    for i,sentence in enumerate(sentences):
        node_id,node_val,node_eow,node_count = retrieveData(T,sentence,delim)
        if node_count not in cnts:
            cnts[node_count] = 0
        cnts[node_count] += 1

    cntsList = [k for k in sorted(cnts)]
    deltaWeights = np.diff(cntsList)

    # print('CNTSL: ',' '.join(['{0:3d} {1:2d}'.format(k,cnts[k]) for k in sorted(cnts)]))
    # print('DLT 1: ','   '.join(['{0:3d} '.format(k) for k in deltaWeights]))

    fAT = firstAboveThresh(deltaWeights,thresh)

    if fAT is not None:
        Temp = Trie()
        counter = 0
        for i,sentence in enumerate(sentences):
            node_id,node_val,node_eow,node_count = retrieveData(T,sentence,delim)
            if node_count >= sorted(cnts)[fAT]:
                sentenceSplit = sentence.split(delim)
                Temp = populateTrie(Temp,sentenceSplit,delim,node_count)
                counter += 1
        del T
        T = copy.deepcopy(Temp)
        del Temp
            
    return T


def trie2Graph(GG,delim):
    trieGraph = nx.DiGraph()
    sentences = list_words(GG.root,delim)
    for sentence in sentences:
        data = retrieveData(GG,sentence,delim)
        words = sentence.split(delim)
        for iword in range(1,len(words)):
            if trieGraph.has_edge(words[iword-1],words[iword]):
                trieGraph[words[iword-1]][words[iword]]['weight'] += data[3]
            else:
                trieGraph.add_edges_from([(words[iword-1],words[iword],{'weight':data[3]})])
    return trieGraph


def graphPlotter0(): # iFrame,numFrames,G0,G1,permutedObsCount,axs):
    fontSize = 10
    node_scale = 0.01
    edge_scale = 0.01
    fig,axs = plt.subplots(1,3,figsize=(15,4))
    axs[0] = fig.add_subplot(1,3,1)
    axs[0].axis('off')
    axs[1] = fig.add_subplot(1,3,2)
    axs[1].axis('off')
    axs[2] = fig.add_subplot(1,3,3)
    axs[2].axis('off')
    plt.ion()
    fig.show()
    fig.canvas.draw()
    return fontSize,node_scale,edge_scale,fig,axs

def graphPlotter1(iFrame,numFrames, G0, G1, permutedObsCount,fontSize,node_scale,edge_scale,fig,axs):
    posStyle = 'neato'  # neato ‘dot’, ‘twopi’, ‘fdp’, ‘sfdp’, ‘circo’
    new_pos = graphviz_layout(G0,prog=posStyle) # ,args='-Gsplines=true') #  -nodesep=0.6 -overlap=False')
    axs[0].clear()
    axs[0].axis('off')
    nx.draw_networkx(G0, ax=axs[0],pos=new_pos, alpha = 0.7,with_labels = True, font_size = fontSize, font_weight='bold')
    axs[0].set_title('Fully Observed Data Step {0:d}'.format(iFrame))
    axs[1].clear()
    axs[1].axis('off')
    nx.draw_networkx(G1, ax=axs[1],pos=new_pos, alpha = 0.7,with_labels = True, font_size = fontSize, font_weight='bold')
    axs[1].set_title('Permuted Data Step {0:d}   Permutations {1:d}'.format(iFrame,permutedObsCount))
    node_color = [G1.degree(v) for v in G1]
    node_size =  [node_scale * nx.get_node_attributes(G1, 'count')[v] for v in G1]
    edge_width = [edge_scale * G1[u][v]['weight'] for u, v in G1.edges()]
    edge_width = 2
    edge_color = [G1[u][v]['weight'] for u, v in G1.edges()]
    axs[2].clear()
    axs[2].axis('off')
    nx.draw_networkx(G1, ax=axs[2],pos=new_pos,node_size = node_size, node_color = node_color, alpha = 0.7,
        with_labels = True, font_size = fontSize, font_weight='bold',width = edge_width, edge_color = edge_color)
    axs[2].set_title('Permuted Data Step {0:d}   Permutations {1:d}'.format(iFrame,permutedObsCount))
    fig.canvas.draw()
    return

def rebalanceTrie(T,delim):
    # Sort the sentences by count so the most common are preferentially considered as keys.  Does this matter?

    sentences = list_words(T.root,delim)
    sentDict = {}
    for i,sentence in enumerate(sentences):
        if sentence not in sentDict:
            permSent = list(itertools.permutations(sentence.split(delim)))
            for j,perm in enumerate(permSent):
                permPhrase = delim.join(['{0:s}'.format(str(r)) for r in perm])
                if permPhrase in sentDict:
                    break
            if permPhrase not in sentDict:
                sentDict[permPhrase] = 0
            sentDict[permPhrase] += retrieveData(T,sentence,delim)[3]
        else:
            sentDict[sentence] += retrieveData(T,sentence,delim)[3]
#
    rebalance = {}
    for k in sentDict:
        fv = k.split(delim)
        for i,v in enumerate(fv):
            if i not in rebalance:
                rebalance[i] = {}
            if v not in rebalance[i]:
                rebalance[i][v] = 0
            rebalance[i][v] += 1
#
    lens = [len(rebalance[i]) for i in rebalance]
    newDict = {}
    for k in sentDict:
        fv = k.split(delim)
        nv = [x for _,x in sorted(zip(lens,fv))]
        newKey = delim.join(['{0:s}'.format(str(r)) for r in nv])
        newDict[newKey] = sentDict[k]
    sentDict = copy.deepcopy(newDict)
#
    Temp = Trie()
    for k in sentDict:
        # print('{0:<30s}'.format(k),sentDict[k])
        Temp = populateTrie(Temp,k.split(delim),delim,sentDict[k])

    return Temp

import matplotlib.gridspec as gridspec

def graphPlotter01():
    fontSize = 10
    node_scale = 0.01
    edge_scale = 0.01

    nrows = 2
    ncols = 6

    fig = plt.figure(constrained_layout=True,figsize=(15,8))
    spec = gridspec.GridSpec(nrows=nrows,ncols=ncols,figure=fig) # wspace=0.25,hspace=0.25)

    axs = []

    axs.append(fig.add_subplot(spec[0,0:2]))
    axs.append(fig.add_subplot(spec[0,2:4]))
    axs.append(fig.add_subplot(spec[0,4:6]))
    axs.append(fig.add_subplot(spec[1,0:3]))
    axs.append(fig.add_subplot(spec[1,3:6]))

    plt.ion()
    fig.show()
    fig.canvas.draw()
    return fontSize,node_scale,edge_scale,fig,axs

def graphPlotter11(iFrame,numFrames, G0, G1, T0, T1, permutedObsCount,partialObsCount,rebalanceCount,fontSize,node_scale,edge_scale,fig,axs,delim):
    posStyle = 'neato'  # neato ‘dot’, ‘twopi’, ‘fdp’, ‘sfdp’, ‘circo’
    new_pos = graphviz_layout(G0,prog=posStyle) # ,args='-Gsplines=true') #  -nodesep=0.6 -overlap=False')
    axs[0].clear()
    axs[0].axis('off')
    nx.draw_networkx(G0, ax=axs[0],pos=new_pos, alpha = 0.7,with_labels = True, font_size = fontSize, font_weight='bold')
    axs[0].set_title('Clean Graph:  Step {0:d}'.format(iFrame))
    axs[1].clear()
    axs[1].axis('off')
    nx.draw_networkx(G1, ax=axs[1],pos=new_pos, alpha = 0.7,with_labels = True, font_size = fontSize, font_weight='bold')
    axs[1].set_title('Noisy Graph:  Perms {0:d}   Partial Obs {1:d}'.format(permutedObsCount,partialObsCount))
    node_color = [G1.degree(v) for v in G1]
    node_size =  [node_scale * nx.get_node_attributes(G1, 'count')[v] for v in G1]
    edge_width = [edge_scale * G1[u][v]['weight'] for u, v in G1.edges()]
    edge_width = 2
    edge_color = [G1[u][v]['weight'] for u, v in G1.edges()]
    axs[2].clear()
    axs[2].axis('off')
    nx.draw_networkx(G1, ax=axs[2],pos=new_pos,node_size = node_size, node_color = node_color, alpha = 0.7,
        with_labels = True, font_size = fontSize, font_weight='bold',width = edge_width, edge_color = edge_color)
    axs[2].set_title('Noisy Graph:   Perms {0:d}   Partial Obs {1:d}'.format(permutedObsCount,partialObsCount))

    posStyle = 'dot'  # neato ‘dot’, ‘twopi’, ‘fdp’, ‘sfdp’, ‘circo’

    tG0 = trie2Graph(T0,delim)
    new_pos = graphviz_layout(tG0,prog=posStyle)
    edge_color = [tG0[u][v]['weight'] for u, v in tG0.edges()]
    axs[3].clear()
    axs[3].axis('off')
    nx.draw_networkx(tG0,ax=axs[3],pos=new_pos,with_labels=True,edge_color=edge_color)
    axs[3].set_title('Clean Trie Step {0:d}'.format(iFrame))


    tG0 = trie2Graph(T1,delim)
    new_pos = graphviz_layout(tG0,prog=posStyle)
    edge_color = [tG0[u][v]['weight'] for u, v in tG0.edges()]
    axs[4].clear()
    axs[4].axis('off')
    nx.draw_networkx(tG0,ax=axs[4],pos=new_pos,with_labels=True,edge_color=edge_color)
    axs[4].set_title('Noisy Trie:   ReBalances {0:d}'.format(rebalanceCount))

    fig.canvas.draw()
    return


def graphPlotterActivity1(iFrame, G0, T0, fontSize,node_scale,edge_scale,fig,axs,delim):
    posStyle = 'twopi'  # neato ‘dot’, ‘twopi’, ‘fdp’, ‘sfdp’, ‘circo’
    new_pos = graphviz_layout(G0,prog=posStyle) # ,args='-Gsplines=true') #  -nodesep=0.6 -overlap=False')
    axs[0].clear()
    axs[0].axis('off')
    nx.draw_networkx(G0, ax=axs[0],pos=new_pos, alpha = 0.7,with_labels = True, font_size = fontSize, font_weight='bold')
    axs[0].set_title('Clean Graph:  Step {0:d}'.format(iFrame))

    posStyle = 'dot'  # neato ‘dot’, ‘twopi’, ‘fdp’, ‘sfdp’, ‘circo’

    tG0 = trie2Graph(T0,delim)
    new_pos = graphviz_layout(tG0,prog=posStyle)
    edge_color = [tG0[u][v]['weight'] for u, v in tG0.edges()]
    axs[1].clear()
    axs[1].axis('off')
    nx.draw_networkx(tG0,ax=axs[1],pos=new_pos,with_labels=True,edge_color=edge_color)
    axs[1].set_title('Clean Trie Step {0:d}'.format(iFrame))

    fig.canvas.draw()
    return
