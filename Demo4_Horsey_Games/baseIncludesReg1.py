import numpy as np
import scipy.special as sc
import cv2 as cv
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu
from scipy import ndimage

import matplotlib.image as mpimg

# import imreg_dft as ird

import imreg_dft_JMK.imreg as ird
import imreg_dft_JMK.utils


from scipy.signal import fftconvolve

def rotate_image(image, angle,borderFill):
    # print(image.shape)
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv.INTER_LINEAR, borderValue=(borderFill))
    return result

def prepImage1(img):
    grayImg = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    thresh = threshold_otsu(grayImg)
    binary = 1-1*(grayImg > thresh)
    comPoint = ndimage.measurements.center_of_mass(binary)

    fovImage = foveate2Point(grayImg,comPoint,255)
    grayImage = np.array(255*(fovImage==255),dtype=np.uint8)

    return grayImage,comPoint

def contImage1(img):
    edgedImage = cv.Canny(img, 0, 255)
    cnts,hier = cv.findContours(edgedImage, cv.RETR_TREE, cv.CHAIN_APPROX_TC89_KCOS)
    longCnt = np.argmax([len(c) for c in cnts])
    contImage = cv.drawContours(255*np.ones_like(img),cnts,-1,(0),1)
    return contImage,cnts,hier,longCnt

def foveate2Point(binaryImage,point,bndVal):
    result = bndVal*np.ones_like(binaryImage)
    center = [np.int(r/2) for r in result.shape]
    point = [np.int(p) for p in point]
    dy,dx = [c-p for p,c in zip(point,center)]
    xmin = np.int(np.max([0,point[1]-center[1]]))
    xmax = np.int(np.min([  point[1]+center[1],binaryImage.shape[1]]))
    ymin = np.int(np.max([0,point[0]-center[0]]))
    ymax = np.int(np.min([  point[0]+center[0],binaryImage.shape[0]]))
    result[ymin+dy:ymax+dy,xmin+dx:xmax+dx] = binaryImage[ymin:ymax,xmin:xmax]
    return result

def plotHelper(img0,img1,img2,img3,angle,scale,result):
    fig,axs = plt.subplots(1,4,figsize=(40,20))
    axs[0].imshow(img0 ,cmap='gray')
    axs[0].set_title('Initial Reference Image')
    axs[1].imshow(img1 ,cmap='gray')
    axs[1].set_title('Rotated Image\nAngle: {0:7.2f}'.format(angle))
    axs[2].imshow(img2 ,cmap='gray')
    axs[2].set_title('Rotated and Scaled Image\nScale: {0:5.3f}'.format(scale))

    ima = np.ones((img0.shape[0],img0.shape[1],3))
    ima[:,:,1] = 1-1*(img0.copy() != 255)
    ima[:,:,2] = 1-1*(img0.copy() != 255)

    imb = np.ones((img3.shape[0],img3.shape[1],3))
    imb[:,:,0] = 1-1*(img3.copy() != 255)
    imb[:,:,1] = 1-1*(img3.copy() != 255)
    imb[:,:,2] = 1-1*(img3.copy() != 255)

    alpha = 0.40
    imnew = cv.addWeighted(ima,alpha,imb,(1.-alpha),0.0)

    axs[3].imshow(imnew ,cmap='viridis')

    titleStr = 'Recovered Image\n' + 'Ang: {0:7.2f}'.format(result['angle'])+ ' '*5
    titleStr += 'Scl: {0:7.2f}'.format(result['scale'])+ ' '*5
    titleStr += 'Trn: ({0:7.2f},{1:7.2f})'.format(result['tvec'][0],result['tvec'][1])
    axs[3].set_title(titleStr)
    plt.show()
    return

def plotHelper2(img0,str0,img1,str1,img2,str2,fSize):
    plt.figure(figsize=fSize)
    plt.plot(img0,label=str0)
    plt.plot(img1,label=str1)
    plt.plot(img2,label=str2)
    plt.legend()
    plt.show()
    return


# https://stackoverflow.com/questions/44650888/resize-an-image-without-distortion-opencv

def image_resize(image, width = None, height = None, inter = cv.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def makeImage(cont,xBnd,yBnd,xDim,yDim,thick):
    x = [c[0][0] for c in cont]
    y = [c[0][1] for c in cont]
    xmin,xmax,ymin,ymax = np.min(x),np.max(x),np.min(y),np.max(y)

    ncont = np.array([[[c[0]-xmin+xBnd,  c[1]-ymin+yBnd]] for c in zip(x,y)],dtype='int32')
    imag = 255*np.ones((ymax-ymin+2*yBnd,xmax-xmin+2*xBnd), np.uint8)
    imag = cv.drawContours(imag,[ncont],-1,color=(0),thickness=thick)
    

    if xmax-xmin > ymax-ymin:
        imag = image_resize(imag,width=xDim)
    if xmax-xmin < ymax-ymin:
        imag = image_resize(imag,height=yDim)
    if xmax-xmin == ymax-ymin:
        imag = cv.resize(imag,(xDim,yDim),interpolation = cv.INTER_AREA)

    imag = 255*(imag==255)

    (h,w) = imag.shape[:2]
    imag2 = 255*np.ones((yDim,xDim), np.uint8)
    
    x0 = (xDim-w)//2
    y0 = (yDim-h)//2
    
    imag2[y0:y0+h,x0:x0+w] = imag[:,:]
    
    return imag2


def extractContours(imag,imgHeight,imgWidth,xBnd,yBnd):

    # Identify contours.
    if 1:
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))
        gray = cv.cvtColor(imag, cv.COLOR_BGR2GRAY)
        edged = cv.Canny(gray, 0, 255)
        dilated = cv.dilate(edged, kernel)
        cnts,hier = cv.findContours(dilated.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    else:
        imagS,cnts,hier,longCnt = contImage1(imag)

    # Save off contours that are longer than 100 points -- totally arbitrary.  There are a lot of short contours.
    shapeDescs = {}
    for icnt,cnt in enumerate(cnts):
        if len(cnt) > 100:
            shapeDescs[icnt] = {}
            x = [c[0][0] for c in cnt]
            y = [c[0][1] for c in cnt]
            xmin,xmax,ymin,ymax = np.min(x),np.max(x),np.min(y),np.max(y)
            newCont = np.array([[[c[0]-xmin+xBnd,  c[1]-ymin+yBnd]] for c in zip(x,y)],dtype='int32')
            shapeDescs[icnt]['cont'] = newCont
            shapeDescs[icnt]['imag'] = makeImage(newCont,2*xBnd,2*yBnd,imgHeight,imgWidth,-1)
            shapeDescs[icnt]['com'] = (np.int(np.mean(x)),np.int(np.mean(y)))

    return shapeDescs

def registerMain(obsv,memr,imgWidth,imgHeight,x0,y0,padVal):

    result = ird.similarity(obsv,memr, numiter=3)

    rec0 = cv.resize(memr.copy(),(np.int(imgWidth*result['scale']),
        np.int(imgHeight*result['scale'])),interpolation = cv.INTER_AREA)

    # Center the recovered scaled image correctly.
    recover = padVal*np.ones_like(obsv)
    if result['scale'] < 1.0:
        i0,j0 = obsv.shape[1]//2-rec0.shape[1]//2,obsv.shape[0]//2-rec0.shape[0]//2
        recover[j0:j0+rec0.shape[0], i0:i0+rec0.shape[1]] = rec0
    else:
        i0,j0 = rec0.shape[1]//2 - x0,rec0.shape[0]//2 - y0
        recover = rec0[j0:j0+obsv.shape[0],i0:i0+obsv.shape[1]]
    recover = rotate_image(recover,result['angle'],padVal)
    return recover,result


def normxcorr2(template, image, mode="full"):
    """
    Input arrays should be floating point numbers.
    :param template: N-D array, of template or filter you are using for cross-correlation.
    Must be less or equal dimensions to image.
    Length of each dimension must be less than length of image.
    :param image: N-D array
    :param mode: Options, "full", "valid", "same"
    full (Default): The output of fftconvolve is the full discrete linear convolution of the inputs.
    Output size will be image size + 1/2 template size in each dimension.
    valid: The output consists only of those elements that do not rely on the zero-padding.
    same: The output is the same size as image, centered with respect to the ‘full’ output.
    :return: N-D array of same dimensions as image. Size depends on mode parameter.
    """

    # If this happens, it is probably a mistake
    if np.ndim(template) > np.ndim(image) or \
            len([i for i in range(np.ndim(template)) if template.shape[i] > image.shape[i]]) > 0:
        print("normxcorr2: TEMPLATE larger than IMG. Arguments may be swapped.")

    template = template - np.mean(template)
    image = image - np.mean(image)

    a1 = np.ones(template.shape)
    # Faster to flip up down and left right then use fftconvolve instead of scipy's correlate
    ar = np.flipud(np.fliplr(template))
    out = fftconvolve(image, ar.conj(), mode=mode)

    image = fftconvolve(np.square(image), a1, mode=mode) - \
            np.square(fftconvolve(image, a1, mode=mode)) / (np.prod(template.shape))

    # Remove small machine precision errors after subtraction
    image[np.where(image < 0)] = 0

    template = np.sum(np.square(template))
    out = out / np.sqrt(image * template)

    # Remove any divisions by 0 or very close to 0
    out[np.where(np.logical_not(np.isfinite(out)))] = 0

    return out

def transObject(tempImg,angle,scale, imgHeight,imgWidth,x0,y0):
    rotImage = rotate_image(tempImg,angle,255)
    sclImage0 = cv.resize(rotImage.copy(),(np.int(scale*imgWidth),np.int(scale*imgHeight)),interpolation = cv.INTER_AREA)
    # Center the scaled image correctly.
    sclImage = 255*np.ones_like(rotImage)
    if sclImage0.shape[0] <= rotImage.shape[0]:
        i0,j0 = x0-sclImage0.shape[1]//2,y0-sclImage0.shape[0]//2
        sclImage[j0:j0+sclImage0.shape[0],i0:i0+sclImage0.shape[1]] = sclImage0
    else:
        i0,j0 = sclImage0.shape[1]//2 - x0,sclImage0.shape[0]//2 - y0
        sclImage = sclImage0[j0:j0+rotImage.shape[0],i0:i0+rotImage.shape[1]]

    newObsImg = sclImage
    newObsCnt,cnts,hier,longCnt = contImage1(newObsImg)

    return(newObsImg,newObsCnt)


def initializeMemoryObject(k1,obsImg,obsCnt):

    learnObj = {}
    learnObj['object'] = k1
    learnObj['mdlimg'] = obsImg  # Initially the model is the observation
    learnObj['mdlcnt'] = obsCnt
    learnObj['matches'] = {}
    learnObj['matches']['params'] = []  # list of tuples (k0,scale,angle,nccc)
    learnObj['matches']['obsimg'] = []  # list of all similar observed images
    learnObj['matches']['obsimg'].append(obsImg)  # Initialize with first observation
    learnObj['matches']['obscnt'] = []  # list of all similar observed contours
    learnObj['matches']['obscnt'].append(obsCnt)  # Initialize with first observation
    learnObj['matches']['regged'] = []  # list of all similar registered images

    return learnObj

def updateMemoryObject(learnObj, obsImg,obsCnt,recovs, params):

    k1,iObs,scale,angle,maxscore = params

#     learnObj['mdlimg'] = obsImg  # Similar observations are used to update the model
#     learnObj['mdlcnt'] = obsCnt

    learnObj['matches']['params'].append((k1,iObs,scale,angle,maxscore))
    learnObj['matches']['obsimg'].append(obsImg)  # add new image to list for this memory model
    learnObj['matches']['obscnt'].append(obsCnt)  # add new contour to list for this memory model
    learnObj['matches']['regged'].append(recovs)  # add the registered memory to this observation

    return learnObj



def buildLexicon(imgHeight,imgWidth,silhouetteFlag,shapeFlag):

    shapeDescs = {}
    if silhouetteFlag:
        #====================================================================================
        # Read in the image
        # img = mpimg.imread('../Data/daisy.jpg')[:-80,:,:]    # 50 Animals
        img = mpimg.imread('../Data/120485.jpg')[:-80,:,:]    # 50 Animals
        # img = mpimg.imread('../Data/1392940.jpg')[:-80,:,:]   # Labeled Dinosaurs
        # img = mpimg.imread('../Data/24077975.jpg')[:-80,:,:]  # Labeled Science Fiction Abilities
        # img = mpimg.imread('../Data/25790030.jpg')[:-80,:,:]  # 100 Animals
        # img = mpimg.imread('../Data/597834.jpg')[:-80,:,:]    # Dinosaurs
        # img = cv.imread('../Data/pokemonsprites.png')

        # Build contours from image
        xBnd,yBnd = imgHeight//8,imgWidth//8  # x and y boundary--a buffer to keep the shape away from edges at each scale
        shapeDescs = extractContours(img,imgHeight,imgWidth,xBnd,yBnd)

        kk = [19,20,25,93,103,352,472,112,249,121]  # Do a selection of shapes (50 animals image input)
    #     kk = [19,20,25,26]
    #     kk = list(shapeDescs.keys())[:10]  # Do the first 10 animals
    #     kk = list(shapeDescs.keys())       # Do all the animals
    
    if shapeFlag:
        imgArea = imgWidth*imgHeight
        minSize = np.sqrt(imgArea*0.05)
        maxSize = np.sqrt(imgArea*0.10)
        shapeKinds = ['rectangle', 'circle', 'triangle', 'ellipse']

        # Build contours from image
        xBnd,yBnd = imgHeight//8,imgWidth//8  # x and y boundary--a buffer to keep the shape away from edges at each scale
        shapeIndex = 0
        shapeDescs = {}
        for shapeKind in shapeKinds:
            for exampleNum in range(2):
                imageRes, labels = random_shapes((imgHeight,imgWidth), max_shapes=1,
                        min_size = minSize, max_size = maxSize, shape=shapeKind, multichannel=True,random_seed=randSeed)
                grayImage0,comPoint = prepImage1(imageRes)
                shapeDescs[shapeIndex] = {}
                shapeDescs[shapeIndex]['imag'] = grayImage0
                shapeIndex += 1

        kk = list(shapeDescs.keys())[:10]  # Do the first 10 shapes
#
    return shapeDescs,kk



# https://github.com/jrosebr1/imutils/blob/master/imutils/object_detection.py

def non_max_suppression(boxes, probs=None, overlapThresh=0.3):
	# if there are no boxes, return an empty list
	if len(boxes) == 0:
		return []

	# if the bounding boxes are integers, convert them to floats -- this
	# is important since we'll be doing a bunch of divisions
	if boxes.dtype.kind == "i":
		boxes = boxes.astype("float")

	# initialize the list of picked indexes
	pick = []

	# grab the coordinates of the bounding boxes
	x1 = boxes[:, 0]
	y1 = boxes[:, 1]
	x2 = boxes[:, 2]
	y2 = boxes[:, 3]

	# compute the area of the bounding boxes and grab the indexes to sort
	# (in the case that no probabilities are provided, simply sort on the
	# bottom-left y-coordinate)
	area = (x2 - x1 + 1) * (y2 - y1 + 1)
	idxs = y2

	# if probabilities are provided, sort on them instead
	if probs is not None:
		idxs = probs

	# sort the indexes
	idxs = np.argsort(idxs)

	# keep looping while some indexes still remain in the indexes list
	while len(idxs) > 0:
		# grab the last index in the indexes list and add the index value
		# to the list of picked indexes
		last = len(idxs) - 1
		i = idxs[last]
		pick.append(i)

		# find the largest (x, y) coordinates for the start of the bounding
		# box and the smallest (x, y) coordinates for the end of the bounding
		# box
		xx1 = np.maximum(x1[i], x1[idxs[:last]])
		yy1 = np.maximum(y1[i], y1[idxs[:last]])
		xx2 = np.minimum(x2[i], x2[idxs[:last]])
		yy2 = np.minimum(y2[i], y2[idxs[:last]])

		# compute the width and height of the bounding box
		w = np.maximum(0, xx2 - xx1 + 1)
		h = np.maximum(0, yy2 - yy1 + 1)

		# compute the ratio of overlap
		overlap = (w * h) / area[idxs[:last]]

		# delete all indexes from the index list that have overlap greater
		# than the provided overlap threshold
		idxs = np.delete(idxs, np.concatenate(([last],
			np.where(overlap > overlapThresh)[0])))

	# return only the bounding boxes that were picked
	return boxes[pick].astype("int")
